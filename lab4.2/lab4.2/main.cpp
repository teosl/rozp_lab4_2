#include <iostream>
#include <cstdlib>
#include <cmath>
#include <windows.h>
#include <ctime>
#include <conio.h>

#define Max_Point 100000


struct point{
	double x;
	double y;
};
double funkcja(double x)//funkcja licz�ca
{
	return 40 * sin(x) + pow(x, 0.75);
}

using namespace std;

#pragma argsused
struct dane_dla_watku // tablica zawiera dane, ktore otrzymaja w�tki
{
	int los;
	double wynik;
	int parametr;
	bool skonczone;
};


DWORD WINAPI funkcja_watku(void *argumenty);
DWORD WINAPI press_key(void *argumenty);




int main(int argc, char **argv)
{
	srand(time(NULL));
	int ilosc = 10;
	int i;
	bool pierwszy = true,next=true;
	DWORD id; // identyfikator w�tku
	printf("Uruchomienie programu\n");
	double sum = 0, dzielnik = 0;
	struct dane_dla_watku *tmp = (struct dane_dla_watku *)malloc(sizeof dane_dla_watku * ilosc);
	HANDLE *watki = (HANDLE *)malloc(sizeof HANDLE * ilosc),guardian;
	
	/*inicjalizacja czuwacza*/
	guardian = CreateThread(NULL, 0, press_key, (void *)&next, 0, &id);
	if (guardian != INVALID_HANDLE_VALUE)
	{
		cout << "utworzono czuwacza!" << endl;
		SetThreadPriority(guardian, THREAD_PRIORITY_ABOVE_NORMAL);
	}

	Sleep(500);
	/*inicjalizacja pocz�tkowa*/
	for (i = 0; i < ilosc; i++)
		tmp[i].skonczone = true;
	
	/*G��wna p�tla*/
	while (next)
	{
		for (i = 0; i < ilosc; i++)
		{
			if (tmp[i].skonczone == true)
			{
				if (pierwszy == false)//�eby przy pierwszym obiegu nie dodawa�o nieistniej�cych wynik�w
				{
					sum += tmp[i].wynik;
					dzielnik++;
					cout << "id: " << tmp[i].parametr << " seed: " << tmp[i].los << " wynik: " << tmp[i].wynik << "srednia: " << sum / dzielnik << endl;
				}
				tmp[i].los = rand()%100;
				tmp[i].parametr = i;
				tmp[i].wynik = 0.0;
				tmp[i].skonczone = false;
				watki[i] = CreateThread(
					NULL, // atrybuty bezpiecze�stwa
					0, // inicjalna wielko�� stosu
					funkcja_watku, // funkcja w�tku
					(void *)&tmp[i],// dane dla funkcji w�tku
					0, // flagi utworzenia
					&id);
				if (watki[i] != INVALID_HANDLE_VALUE)
				{					
					SetThreadPriority(watki[i], THREAD_PRIORITY_NORMAL);
				}
			}
		}	
		pierwszy = false;
		
	}
	return 0;
}


/*watek zajmuj�cy si� obliczeniami*/
DWORD WINAPI funkcja_watku(void *argumenty)
{
	struct dane_dla_watku *moje_dane = (struct dane_dla_watku *)argumenty;
	struct point tmpp;

	//moje_dane->los = moje_dane->los*moje_dane->parametr % 50;

	srand(moje_dane->los);//ustawienie seeda na wczesniej wylosowany numer
	double suma = 0, tmp;
	for (int i = 0; i < Max_Point; i++)
	{
		tmpp.x = (double)rand() * 40.0 / RAND_MAX;
		tmpp.y = (double)rand() * 80.0 / RAND_MAX;
		tmp = funkcja(tmpp.x);
		if (tmp > 0)
		{
			if (tmp>tmpp.y)
				suma += 1;
		}
		else
		{
			if (abs(tmp)>tmpp.y)
				suma -= 1;
		}
	}
	moje_dane->wynik = suma / Max_Point * 40 * 80;
	Sleep(moje_dane->los*10);//losowe u�pienie �eby "urealni�" program
	moje_dane->skonczone = true;
	return 0;
}

/*W�tek czekaj�cy na wcisni�cie klawisza*/
DWORD WINAPI press_key(void *argumenty)
{
	bool *next = (bool *)argumenty;
	char a;
	_getch();
	*next = false;
	return 0;
}